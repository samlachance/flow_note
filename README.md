# Flow Note

## About

Flow Note allows you to quickly take notes in the command line. 

## Installation

```
gem install flow_note
```

## Setup

Flow Note requires a `FLOW_DIR` env var so it knows where to store the notes. Something like this:

```
mkdir ~/.flow
```

You'll also need to set the env var by exporting the path in your `.bashrc` or equivalent.

```
export FLOW_DIR=/home/sam/.flow
```


## Usage

### Take a note

```
flow note grocery list: eggs, milk, coffee, bread, cheese
```

Once a note is created, Flow Note writes the note to a file and stores it in the `FLOW_DIR`. The filename is the iso8601 date of the note creation.


### Read your notes

```
flow show
```

Flow Note will generate an HTML file with your notes and then launch it in a browser. To modify the styles, add `style.css` to your `FLOW_DIR`.
