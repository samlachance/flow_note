Gem::Specification.new do |s|
  s.name        = 'flow_note'
  s.version     = '0.0.1'
  s.date        = '2019-10-28'
  s.summary     = 'Flow'
  s.description = 'CLI note taking app'
  s.authors     = ['Sam Lachance']
  s.email       = 'sam@samlachance.com'
  s.homepage    =
    'https://samlachance.com'
  s.license       = 'MIT'

  s.bindir = "bin"
  s.files       = Dir["{bin,lib}/**/*"]
  s.executables = %w(flow)

  s.add_dependency 'thor', '~> 0.20.3'
  s.add_dependency 'launchy', '~> 2.4', '>= 2.4.3'
end
