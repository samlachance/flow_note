require 'thor'
require 'launchy'

class FlowNote < Thor
  FLOW_DIR=ENV['FLOW_DIR']

  desc 'note BODY', 'Record a note'
  def note(*body)
    File.open("#{FLOW_DIR}/#{Time.now.strftime("%Y-%m-%d")}.md", 'a') do |f|
      f.write "[#{Time.now} | #{body.join(' ')}]"
    end
  end

  desc "show", "View your notes"
  def show
    build
    Launchy.open("file://#{FLOW_DIR}/show.html")
  end

  private

  def build
    notes = Dir.glob("#{FLOW_DIR}/*.md").sort[0..99]
    
    buffer = StringIO.new
    buffer << "<html><head><link rel='stylesheet' type='text/css' href='style.css'></head><body>"
    notes.each do |note|
      buffer << '<div class="note"><p>'
      buffer << File.read(note)
      buffer << '</p></div>'
    end
    buffer << '</body></html>'

    File.open("#{FLOW_DIR}/show.html", 'w') do |f|
      f.write buffer.string
    end
  end
end
